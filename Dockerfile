FROM ubuntu:19.10

RUN dpkg --add-architecture i386 && apt-get -qq update && apt-get install -y git unzip \
make cmake scons lib32z1 lib32ncurses6 \
libc6:i386 libncurses5:i386 libstdc++6:i386 && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/local/bin/NXP

RUN git clone -q https://bitbucket.org/rtmalim/s32klinuxtoolchain.git && mv s32klinuxtoolchain/* . \
	&& rm -rf s32klinuxtoolchain \
    && unzip -q build-wrapper-linux-x86.zip \
    && unzip -q sonar-scanner-cli-4.3.0.2102-linux.zip \
    && rm build-wrapper-linux-x86.zip \
    && rm sonar-scanner-cli-4.3.0.2102-linux.zip

ENV PATH $PATH:/usr/local/bin/NXP/gcc-6.3-arm32-eabi/bin:/usr/local/bin/NXP/build-wrapper-linux-x86:/usr/local/bin/NXP/sonar-scanner-4.3.0.2102-linux/bin

WORKDIR /usr/project
#WORKDIR /home/rtmalim/develop/docker/tcu_test_build/tcu

#CMD build-wrapper-linux-x86-64 --out-dir bw_output scons -j4 -Q build=RELEASE
#CMD build-wrapper-linux-x86-64 --out-dir TCU/bw_output cmake -S TCU -B TCU/build -DCMAKE_BUILD_TYPE=Release && make -C TCU/build -j4
CMD echo "Usage ex for TCU:\n1. cmake -S TCU -B TCU/build -DCMAKE_BUILD_TYPE=Release\n2. build-wrapper-linux-x86-64 --out-dir TCU/bw_output make -C TCU/build -j4\n3. sonar-scanner -D\"sonar.projectKey=Athena_TCU\" -D\"sonar.sources=.\" -D\"sonar.host.url=http://sonarqube.rototilt.com:9000\" -D\"sonar.login=23934c9d01eab0a018925333c5f55d4317c3e887\""
